import numpy as np
import matplotlib.pyplot as plt
import itertools
from scipy import ndimage
from scipy import stats
import os
cwd = os.getcwd()

def ab2xyz(alfa,beta):
    x= np.cos(alfa) * np.sin(beta)
    y= np.sin(alfa) * np.sin(beta)
    z = np.cos(beta)
    return x,y,z

def xyz2ab(x,y,z):
      beta=np.arccos(z)
      alfa1=np.arccos(x/np.sin(beta)) 
      a1=np.where(abs(y-(np.sin(alfa1) * np.sin(beta)))< 1e-8,1,0)
      a2=np.where(abs(y-(np.sin(alfa1+np.pi) * np.sin(beta)))< 1e-8,1,0)
      alfa=a1*alfa1+a2*(alfa1+np.pi)
      return alfa,beta

def Guassian_dist(NPoints,Sigma,Alf0):
      # Generate NPoints Normally distributed along (001) direction
      # Also checking if the distribution peack isn't too sharp or broad
      DAlfa=Alf0[1]-Alf0[0]
      # Area of each Segment
      Alfa_Area=2*np.pi*(1**2)*(np.cos(Alf0)-np.cos(Alf0+DAlfa))
      # Corresponding x in mid Alfa
      Alfa_x=1*np.sin(Alf0+DAlfa/2)
      SigmaFactor=0
      while SigmaFactor!=1:
            # First, Calculating density of points regarding Gaussian distribution, along x=0 to r
            Rho_max=1/(np.sqrt(2*np.pi)*Sigma)
            Rho_factor=NPoints / (Sigma*np.sqrt(2*np.pi)*(1-np.exp((-0.5)*(1/Sigma)**2)))            
            # Density
            Rho= Rho_factor * Rho_max*np.exp((-0.5)*(Alfa_x/Sigma)**2)            
            # Number of points to generate in that segment
            Alfa_N=np.round(Alfa_Area*Rho,0)
            # Sigma Check:
            SigmaFactor=1      
            if Alfa_N[1]==0:              SigmaFactor=1/(0.8)    # The peak is too sharp
            if Alfa_N[-1]/NPoints>0.0002: SigmaFactor=0.8  # The peak is too broad   
            if SigmaFactor!=1 : print('Sigma Changed from > to: ',Sigma, '>', Sigma*SigmaFactor)
            Sigma=Sigma*SigmaFactor   
      # Making sure the number of generated points is exactly NPoints, if Not fixing with weighted amounts
      Alfa_N=Alfa_N+np.round(Alfa_N * ((NPoints-sum(Alfa_N))/sum(Alfa_N)) ,0) 
      Alfa_N[np.where(Alfa_N==max(Alfa_N))[0][0]] += NPoints-sum(Alfa_N)      
      return Alfa_N.astype(int)

def Euler2Rot(Ang,flag):
    if flag=='deg':
        Ang=np.deg2rad(Ang)        
    Q0=np.zeros(shape=(3,3))
    Q0[0,0] = np.cos(Ang[0])*np.cos(Ang[2])-np.sin(Ang[0])*np.cos(Ang[1])*np.sin(Ang[2])
    Q0[0,1] = np.sin(Ang[0])*np.cos(Ang[2])+np.cos(Ang[0])*np.cos(Ang[1])*np.sin(Ang[2])
    Q0[0,2] = np.sin(Ang[1])*np.sin(Ang[2])
    Q0[1,0] =-np.cos(Ang[0])*np.sin(Ang[2])-np.sin(Ang[0])*np.cos(Ang[1])*np.cos(Ang[2])
    Q0[1,1] =-np.sin(Ang[0])*np.sin(Ang[2])+np.cos(Ang[0])*np.cos(Ang[1])*np.cos(Ang[2])
    Q0[1,2] = np.sin(Ang[1])*np.cos(Ang[2])
    Q0[2,0] = np.sin(Ang[0])*np.sin(Ang[1])
    Q0[2,1] =-np.cos(Ang[0])*np.sin(Ang[1])
    Q0[2,2] = np.cos(Ang[1])
    return Q0

def Rot2Euler(Q,flag):
      Ang=np.zeros(3)
      if (abs(Q[2,2])<1):
        Ang[1] = np.arccos(Q[2,2])
        STH    = np.sin(Ang[1])
        Ang[0] = np.arctan2(Q[2,0]/STH,-Q[2,1]/STH)
        Ang[2] = np.arctan2(Q[0,2]/STH,Q[1,2]/STH)
      else:
        Ang[0] = np.arctan2(Q[0,1],Q[0,0])
        Ang[1] = 0
        Ang[2] = 0
      Ang[0] += np.pi ; Ang[2] += np.pi
      if flag=='deg':
          Ang=np.rad2deg(Ang)          
      return Ang

def rotation_matrix_from_vectors(vec1, vec2):
    """ Find the rotation matrix that aligns vec1 to vec2
    :param vec1: A 3d "source" vector
    :param vec2: A 3d "destination" vector
    :return mat: A transform matrix (3x3) which when applied to vec1, aligns it with vec2.
    """
    a, b = (vec1 / np.linalg.norm(vec1)).reshape(3), (vec2 / np.linalg.norm(vec2)).reshape(3)
    v = np.cross(a, b)
    c = np.dot(a, b)
    s = np.linalg.norm(v)
    kmat = np.array([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])
    rotation_matrix = np.eye(3) + kmat + kmat.dot(kmat) * ((1 - c) / (s ** 2))
    return rotation_matrix

def Get_Random_Eulers(NR):
      M = stats.special_ortho_group.rvs(dim=3,size=NR)
      if NR==1:
        angs= Rot2Euler(M,'deg')
      else:
        angs= [Rot2Euler(M[i,:,:],'deg') for i in range(0,NR)]   
      # Another Approach to test:
      # Euler angle orientation data of random texture can be generated as (phi1,PHI,phi2)=(2pi*r1,cos-1(1-2*r2),2pi*r3),
      # where r1, r2 and r3 can be the computer generated random numbers in the interval [0,1].
      return angs

def Get_Homogenous_Eulers(NR):  
      # Generate NR evenly distributed points on the whole range
      indices = np.arange(0, NR, dtype=float) + 0.5
      psi = np.arccos(1 - 2*indices/NR)
      alfa = np.pi * (1 + 5**0.5) * indices
      alfa = np.mod(alfa,2*np.pi)
      # psi in range [0,180] (> Phi), alfa in range [0,360] (>phi1)
      phi1 = np.rad2deg(alfa) ; Phi = np.rad2deg(psi)
      #phi2 = np.rad2deg(np.linspace(0,2*np.pi,NR, endpoint=False))
      phi2=np.rad2deg(np.random.uniform(0,2*np.pi,NR))
      #delete x, y, z= ab2xyz (alfa,beta) 
      #deletepts=np.zeros(shape=(4,len(x)))
      #deletepts[0], pts[1], pts[2]=x , y , z       
      #deletePlotScatter3d(pts)
      return np.transpose(np.array([phi1,Phi,phi2]))

def Reduce_With_Sym(Eulers):
      #Phi1:
      syms=np.array([90,180,270,360])
      for i in range(len(syms)-1):
            indxs=np.where((Eulers[:,0]>syms[i]) & (Eulers[:,0]<=syms[i+1]))[0]
            Eulers[indxs,0] += -syms[i]
      #Phi:
      syms=np.array([90,180])
      for i in range(len(syms)-1):
            indxs=np.where((Eulers[:,1]>syms[i]) & (Eulers[:,1]<=syms[i+1]))[0]
            Eulers[indxs,1] += -syms[i]
      #Phi2:
      syms=np.array([90,180,270,360])
      for i in range(len(syms)-1):
            indxs=np.where((Eulers[:,2]>syms[i]) & (Eulers[:,2]<=syms[i+1]))[0]
            Eulers[indxs,2] += -syms[i] 
      return Eulers

def Get_Textured_Eulers(NR,Sigma,Texture):
      print(NR,Sigma,Texture)
      '''
      Sigma=0.1 # Larger Sigma, broader distribution
      # Generate NPoints distributed along (001) direction, with Guassian distribution
      # Defining N_bins-1 Angle range
      N_bins=40+1
      Ang_list=np.linspace(0, np.pi/2, num=N_bins)[0:-1]
      DAng=Ang_list[1]-Ang_list[0]
      Alfa_N = Guassian_dist(NR,Sigma,Ang_list)  
      for Ai,Ang in enumerate(Ang_list):
            beta=np.random.uniform(low=Ang, high=Ang+DAng, size=(Alfa_N[Ai],))
            alfa=np.random.uniform(low=0,   high=2*np.pi , size=(Alfa_N[Ai],))
            x1, y1, z1=ab2xyz(alfa,beta)
            x=np.concatenate((x, x1), axis=0)
            y=np.concatenate((y, y1), axis=0)
            z=np.concatenate((z, z1), axis=0) 
      '''           
      return 0      

def Projection (Eulers,PF_plane,proj_mode):    
      Family= GetFamilyOfPlanes(PF_plane)  
      Prj=np.zeros(2)
      Prjs= np.empty((0,2)) 
      for Euleri in Eulers:
            Qi=Euler2Rot(Euleri,'deg')            
            for planei in Family:
                  # Rotating plane normal vector with Qi            
                  planei=np.matmul(planei,Qi)               
                  if planei[2]>=0:  
                        # Conversion from Cartesian to Spherical coordinates
                        alpha = np.arctan2(planei[1],planei[0])
                        psi = np.arccos(planei[2])                        
                        Prj[0] , Prj[1] = Project(alpha, psi, proj_mode)  
                        Prjs = np.append(Prjs, np.array([Prj]), axis=0)        
      return np.transpose(Prjs) 

def Points2Dens(Points,MS,MSx,MSy):
    # np.save('points.npy',Points)
    Dens=np.zeros(shape=(MS+1,MS+1))
    inx,iny= MS/2+np.round(Points[0]* MS/2,0) , MS/2+np.round(Points[1]* MS/2,0)
    inx,iny = inx.astype(int),iny.astype(int) 
    inxy=np.transpose(np.vstack((inx,iny)))
    unique, counts = np.unique(inxy, axis=0,return_counts=True)
    Dens[unique[:,0],unique[:,1]]=counts

    # Correcting density for the cells that intersect the circle
    #for i, j in itertools.product(range(MS+1), range(MS+1)):

      
    # Normalizing density so that (integral of Density)/(Area of projection disk)=1
    Dens=Dens /np.sum(Dens) * (np.pi*(MS/2)**2) 
    # Smoothing 
    # resampling data grid by using cubic spline interpolation
    pw = 3
    xm = ndimage.zoom(MSx, pw)
    ym = ndimage.zoom(MSy, pw)
    zm = ndimage.zoom(Dens, pw)
    zm = np.where(zm<0, 0, zm)
    return xm, ym, np.transpose(zm)

def Points2Dens3D(Points,MS):
    Scale=90/MS
    Dens=np.zeros(shape=(MS,MS,MS))
    inx,iny,inz= np.floor(Points[0]/Scale) , np.floor(Points[1]/Scale) , np.floor(Points[2]/Scale)
    inx,iny,inz = inx.astype(int),iny.astype(int) ,inz.astype(int)
    inx[np.where(inx==MS)[0]]=MS-1 ; iny[np.where(iny==MS)[0]]=MS-1; inz[np.where(inz==MS)[0]]=MS-1

    inxyz=np.transpose(np.vstack((inx,iny,inz)))
    unique, counts = np.unique(inxyz, axis=0,return_counts=True)
    Dens[unique[:,0],unique[:,1],unique[:,2]]=counts
      
    # Normalizing density so that (integral of Density)/(Volume)=1
    Dens=Dens /np.sum(Dens) * (MS**3) 
    return np.transpose(Dens)

def Points2Dens2(phi1,phi2,MS,MSx,MSy):
    Points=phi1
    Points=np.vstack((Points,phi2))        
    Scale=90/MS
    Dens=np.zeros(shape=(MS+1,MS+1))
    inx,iny= np.round(Points[0]/Scale,0) , np.round(Points[1]/Scale,0)
    inx,iny = inx.astype(int),iny.astype(int) 
    inxy=np.transpose(np.vstack((inx,iny)))
    unique, counts = np.unique(inxy, axis=0,return_counts=True)
    Dens[unique[:,0],unique[:,1]]=counts
      
    # Normalizing density so that (integral of Density)/(Area of projection disk)=1
    Dens=Dens /np.sum(Dens) * (MS**2) 
    # Smoothing 
    # resampling data grid by using cubic spline interpolation
    pw = 2
    xm = ndimage.zoom(MSx, pw)
    ym = ndimage.zoom(MSy, pw)
    zm = ndimage.zoom(Dens, pw)
    zm = np.where(zm<0, 0, zm)
    return xm, ym, np.transpose(zm)

def Eulers2Dens(Points,MS,MSx,MSy):
    Dens=np.zeros(shape=(MS+1,MS+1))
    inx,iny= MS/2+np.round(Points[0]* MS/2,0) , MS/2+np.round(Points[1]* MS/2,0)
    inx,iny = inx.astype(int),iny.astype(int) 
    inxy=np.transpose(np.vstack((inx,iny)))
    unique, counts = np.unique(inxy, axis=0,return_counts=True)
    Dens[unique[:,0],unique[:,1]]=counts
    
    # Normalizing density so that (integral of Density)/(Area of projection disk)=1
    Dens=Dens /np.sum(Dens) * (np.pi*(MS/2)**2) 
    # Smoothing 
    # resampling data grid by using cubic spline interpolation
    pw = 3
    xm = ndimage.zoom(MSx, pw)
    ym = ndimage.zoom(MSy, pw)
    zm = ndimage.zoom(Dens, pw)
    zm = np.where(zm<0, 0, zm)
    return xm, ym, np.transpose(zm)

def Project(alpha, psi, proj_mode):
      if (psi>(np.pi/2)): psi = np.pi - psi
      c_alpha,s_alpha=np.cos(alpha), np.sin(alpha)
      if proj_mode=='Steriographic_Projection':      
            R=1      
            Rtan = R*np.tan(psi/2)
            x,y= Rtan*c_alpha , Rtan*s_alpha
      else:
            R=1/np.sqrt(2)
            Rsin = 2*R*np.sin(psi/2)
            x,y= Rsin*c_alpha , Rsin*s_alpha
      return y,x

def GetFamilyOfPlanes(plane):
    c=[]
    a = np.array(list(itertools.permutations(plane)))
    #    b = [[1,1,1],[-1,1,1],[1,-1,1],[-1,-1,1]]
    b = [[1,1,1],[-1,1,1],[1,-1,1],[1,1,-1],[-1,-1,1],[1,-1,-1],[-1,1,-1],[-1,-1,-1]]
    for ai in a:
        for bi in b:
            ci=ai*bi
            # noramlizing the vector length
            ci = ci/np.linalg.norm(ci)         
            c.append(ci)             
    return np.unique(c, axis=0) 
      