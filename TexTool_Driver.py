# Generating GUI is done in TexTool_GUI.py
# Funcions used are in TexTool_Funcs.py
#from pyexpat.errors import XML_ERROR_ATTRIBUTE_EXTERNAL_ENTITY_REF
#from tkinter.tix import DirSelectDialog
#from xml.dom import xmlbuilder
from TexTool_GUI import *
import TexTool_Funcs as Funcs
import GenTex as GenTex
import numpy as np
import copy
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.patches as patches
from matplotlib.collections import PolyCollection
#from mpl_toolkits.mplot3d import Axes3D 
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import os
from PyQt5 import QtCore, QtGui, QtWidgets
#plt.rcParams['text.usetex'] = True
cwd = os.getcwd()
# Global array holding the generated orientations
EulersAll=[]   
FCC_Texture_Comps={ 'Cube (C)'            : '0,0,0',
                    'Rotated Cube (RC)'   : '0,0,45',
                    'Copper (CU)'         : '90,35,45',
                    'Rotated Copper (RCu)': '0,35,45',
                    'Inverse Copper (ICu)': 'x,x,x',
                    'Copper Twin (CuT)'   : '90,74,45', 
                    'Brass (B)'           : '55,90,45',
                    'Inverse Brass (IB)'  : '0,35,45',
                    'Goss (G)'            : '90,90,45',
                    'Rotated Goss (RG)'   : '0,90,45',
                    'Goss Twin (GT)'      : '90,25,45',
                    'S'                   : '59,37,63',
                    'A'                   : '35,90,45',
                    'G/B'                 : '70,90,45'}

# Temporary>
ImportFileName=cwd +'\SampleTextures\Texture_100_111.dat'

def AddItem_BTN(self):
    itm_type=self.ItemsCombo.currentText()
    if itm_type=='Random Distribution': add_type=itm_type
    if itm_type=='Texture Component':   add_type='> ' + self.TextureCombo.currentText()
    #if itm_type=='Asymmetrical Fiber':  add_type='| '
    #if itm_type=='Beta Fiber':          add_type='~ '
    rowPosition = self.tableWidget.rowCount()
    self.tableWidget.insertRow(rowPosition)
    # Reading Inputs
    Ori_NO=self.OriNoTXT.text()
    Sigma=self.SigmaTXT.text() if add_type!='Random Distribution' else '-'
    Texture=self.TextureTXT.text()  if add_type!='Random Distribution' else '-'     
    # Making Cells
    cell0 = QtWidgets.QTableWidgetItem(add_type)
    cell1 = QtWidgets.QTableWidgetItem(Ori_NO)
    cell2 = QtWidgets.QTableWidgetItem(Sigma)
    cell3 = QtWidgets.QTableWidgetItem(Texture)
    # Making some cells Not editable
    cell0.setFlags(QtCore.Qt.ItemIsEnabled)
    if itm_type=='Random Distribution':
        cell2.setFlags(QtCore.Qt.ItemIsEnabled)
        cell3.setFlags(QtCore.Qt.ItemIsEnabled)
    # Adding created cells to the table widget
    self.tableWidget.setItem(rowPosition , 0, cell0)
    self.tableWidget.setItem(rowPosition , 1, cell1)      
    self.tableWidget.setItem(rowPosition , 2, cell2)
    self.tableWidget.setItem(rowPosition , 3, cell3) 
    # To avoid adding the same item twice
    self.AddItemBTN.setEnabled(False)

def Items_Combo(self):
    # To handle the Items combo change event
    #'''
    self.AddItemBTN.setEnabled(True)
    itm_type=self.ItemsCombo.currentText()    
    if itm_type=='Random Distribution':
        self.AddItemFRM.setVisible(False)
        # To avoid adding Random item twice
        exists=False
        for i in range(self.tableWidget.rowCount()):
            if self.tableWidget.item(i,0).data(0)==itm_type: exists=True    
        self.AddItemBTN.setEnabled(not exists)    
    if itm_type=='Texture Component':
        self.AddItemFRM.setVisible(True)
        self.TextureCombo.setCurrentText("Cube (C)")
        self.SigmaTXT.setText('3.0')
        self.TextureTXT.setText('0,0,0')
    if itm_type=='Asymmetrical Fiber':
        self.AddItemFRM.setVisible(False)
        self.AddItemBTN.setEnabled(False)
    if itm_type=='Beta Fiber':
        self.AddItemFRM.setVisible(False)
        self.AddItemBTN.setEnabled(False)
    # '''

def Texture_Combo(self):
    # To handle the Texture combo change event
    Texture_type=self.TextureCombo.currentText()
    self.TextureTXT.setText(FCC_Texture_Comps.get(Texture_type, '0,0,0')) 
    # To avoid adding the same texture twice
    self.AddItemBTN.setEnabled(True)
    exists=False
    for i in range(self.tableWidget.rowCount()):
        if self.tableWidget.item(i,0).data(0)[2:]==Texture_type: exists=True    
    self.AddItemBTN.setEnabled(not exists)    

def TableWidget_Slcted(self):
    print('SELECTED')     

def openFileNameDialog():
    options = QtWidgets.QFileDialog.Options()
    fileName, _ = QtWidgets.QFileDialog.getOpenFileName(None,"Opening file", "","Text Files (*.dat;*.txt)", options=options)
    if fileName:  return fileName

def saveFileNameDialog():
    options = QtWidgets.QFileDialog.Options()
    fileName, _ = QtWidgets.QFileDialog.getSaveFileName(None,"Saving into text file", "","Text Files (*.txt)", options=options)
    if fileName:  return fileName

def Import_BTN(self):
    global ImportFileName 
    fname = openFileNameDialog()       
    if fname!='':
        ImportFileName = fname
        Eulers_Loaded = np.loadtxt(ImportFileName)
        self.ImportFileLBL.setText(fname.split('/')[-1])
        self.ImportedTotal.setText(str(len(Eulers_Loaded)))
        self.ImportedInclude.setText(str(len(Eulers_Loaded)))

def Export_BTN(self):
    # Exporting Euler Angles as a .txt file
    if len(EulersAll)>0:
        fname = saveFileNameDialog()       
        if fname!='':
            np.savetxt(fname, EulersAll, fmt='%1.4f')
    else:
        print ("No points to export")


def Imported_Include(self):
    IncludeNotxt = self.ImportedInclude.text()
    TotalNo = int(self.ImportedTotal.text())
    print('txt',IncludeNotxt)   
    if IncludeNotxt.isnumeric():              
        IncludeNo = int(IncludeNotxt)   
        print(IncludeNo)            
        if IncludeNo <= 0 or IncludeNo > TotalNo :   self.ImportedInclude.setText(str(TotalNo))            
    else:
        self.ImportedInclude.setText(str(TotalNo))

def Generate_BTN(self):
    self.Prgrsbar.setProperty("value", 0)    
    global EulersAll
    # Plot Settings
    PF_planes =  Get_PF_planes()  
    mode= 'Point_Projection' if self.actionPoint_Projection.isChecked() else 'Density_Map'
    proj_mode= 'Steriographic_Projection' if self.actionSteriographic_Projection.isChecked() else 'Area_Projection'
    # Loading Table data into array
    tbl=np.array([])
    for i in range(self.tableWidget.rowCount()):
        row=''
        for j in range(4):
            row += self.tableWidget.item(i,j).data(0) + '|'
        tbl=np.append (tbl,row)   
    Total_Pts=0
    for row in tbl:
        Total_Pts += int(row.split('|')[1])
    # Generating Eulers according to table content
    EulersAll=np.zeros(shape=(1,3))
    Generated_Pts=0    
    for row in tbl:
        row_=row.split('|')        
        if int(row_[1])>0:            
            ttle= row_[0]            
            EulersNo=int(row_[1])
            if row_[2]!='-': Sigma=float(row_[2]) 
            if row_[3]!='-': Texture=row_[3]
            ##if ttle == 'Homogenous Dis tribution':
            ##    Eulersi= Funcs.Get_Homogenous_Eulers(EulersNo)
            ##    EulersAll =np.vstack((EulersAll,Eulersi))
            self.statusbar.showMessage('Generating %d Orientations: %s' %(EulersNo,ttle))
            if ttle=='Random Distribution':
                Eulersi= Funcs.Get_Random_Eulers(EulersNo)
                ### Eulersi= GenTex.RANDGEN(EulersNo)
                EulersAll =np.vstack((EulersAll,Eulersi))   
            if ttle[0] == '>': # Texture
                ###Eulersi= Funcs.Get_Textured_Eulers(EulersNo,Sigma,Texture)
                Texture_=np.array(Texture.split(',')).astype(float)
                Eulersi= GenTex.USERTEX(EulersNo, Texture_, Sigma, False)         
                EulersAll =np.vstack((EulersAll,Eulersi))                        
            if ttle == 'Asymmetrical Fiber':                
                print('generate ',ttle)   
            if ttle == 'Beta Fiber':
                print('generate ',ttle)
            # Updating progressbar
            Generated_Pts +=EulersNo
            prog=int(100*Generated_Pts/Total_Pts)
            if prog>100: prog=100
            self.Prgrsbar.setProperty("value", prog)         
    # Updating statusbar
    if len(EulersAll)>1:
        self.statusbar.showMessage('%d Orientations generated in total.' %len(EulersAll[1:]))
    # Including Euler angles imported from file, if requested
    if self.Include_external_CHK.isChecked():
        Eulersi_ = np.loadtxt(ImportFileName) 
        TotalNo = int(self.ImportedTotal.text())            
        IncludeNotxt = int(self.ImportedInclude.text())            
        if IncludeNotxt==TotalNo:
            Eulersi = Eulersi_
        else:
            Rand_indx = np.random.choice(range(IncludeNotxt), size=IncludeNotxt, replace=False) 
            Eulersi = Eulersi_[Rand_indx,:]    
        if len(Eulersi)>0: EulersAll = np.vstack((EulersAll,Eulersi))  
    # Randomly suffling the angles array
    if len(EulersAll)>1:
        # Removing the first row, of zeros
        EulersAll=EulersAll[1:]
        rows = np.arange(len(EulersAll))
        np.random.shuffle(rows)
        EulersAll=EulersAll[rows]
    
    # Making Pole Figures
    if len(EulersAll)>0:        
        self.statusbar.showMessage('Plotting Pole Figure for %d Orientations...' %len(EulersAll))
        Plot_PF(self,PF_planes,mode,proj_mode)
        self.statusbar.showMessage('Pole Figure plotted for %d Orientations.' %len(EulersAll))
    else:
        self.statusbar.showMessage('No Points to plot')


def Plot_PF(self,PF_planes,mode,proj_mode):    
    fig=plt.figure(figsize=(18,6),dpi=200)
    if mode=='Density_Map':
        # ColorMap for contour
        cmapx = copy.copy(matplotlib.cm.get_cmap(choosen_cmap(self)))             
        cmapx.set_under('w')   
        # Density Mesh
        MS=20 # Number of Grids in each direction for Density mesh        
        MSxy= np.linspace(-1,1,MS+1)
        MSx,MSy = np.meshgrid(MSxy,MSxy)
    for i,PF_plane in enumerate(PF_planes):
        ax=fig.add_axes([i*0.33,0,0.33, 1])
        Points=Funcs.Projection(EulersAll,PF_plane,proj_mode)                
        if mode=='Point_Projection':
            ax.scatter(Points[0],Points[1],marker='x',s=1)       
        if mode=='Density_Map':                                   
            xm,ym,Density= Funcs.Points2Dens(Points,MS,MSx,MSy)
            maxv= np.amax(Density) ; minv= np.amin(Density) ; avv= np.average(Density)
            print('Plane, max,min,av denisty:',PF_plane, maxv,minv,avv*np.sqrt(2))
            lvls=np.linspace(0,maxv,10+1)                        
            cs=ax.contourf(xm,ym,Density, len(lvls), vmin=0, cmap=cmapx,extend='min') 
            cbar=plt.colorbar(cs, ax=ax, shrink=0.6) 
            cbar.ax.tick_params(labelsize=10) 
            #line_colors = ['black' for l in cs.levels]  
            #ax.contour(cs, levels=lvls, colors=line_colors)  #or cmap=cmapx
            ax.patch.set_facecolor('xkcd:mint green')
        ax.axis('off') 
        ax.set_aspect('equal') 
        Plane_txt='(' + str(PF_plane[0]) + str(PF_plane[1]) + str(PF_plane[2]) + ')'
        ax.set_title( Plane_txt, fontsize=14, y=1.02)              
        circle1 = plt.Circle((0, 0), 1, color='black', fill=0)
        ax.add_artist(circle1)
        ax.set_xlim([-1.1,1.1]) ; ax.set_ylim([-1.1,1.1])
        ax.text(-0.03,1.02,'RD',fontsize=12) ; ax.text(1.02,-0.02,'TD',fontsize=12) 
        if mode=='Density_Map':
            circ = patches.Circle((0, 0), 1, transform=ax.transData)
            for coll in cs.collections:
                coll.set_clip_path(circ)  
                      
    fig.savefig('Pole_Figure.png')    
    # adding image to GUI
    self.pixmap = QtGui.QPixmap('Pole_Figure.png')      
    self.Pic_WDG1.setPixmap(self.pixmap)
    self.Pic_WDG1.setScaledContents(True)

def Plot_ODF(self,mode):  
    Eulers=Funcs.Reduce_With_Sym(EulersAll)
    fig=plt.figure(figsize=(20,6),dpi=200) 
    Nsec=18 ; DSec=90/Nsec
    Sections=np.linspace(0,90-DSec,Nsec)
    if mode=='Density_Map':
        # ColorMap for contour
        cmapx = copy.copy(matplotlib.cm.get_cmap('jet'))             
        cmapx.set_under('w')   
        # Density Mesh
        MS=10 # Number of Grids in each direction for Density mesh: [90 or 45 or 30 or 18 or 15 or 10]
        MSxy= np.linspace(0,90,MS+1)
        MSx,MSy = np.meshgrid(MSxy,MSxy)    
    
    phi1s=[];phi2s=[]
    xms=[]; yms=[]; Densitys=[]
    maxvOfall=0
    for i,Sec in enumerate(Sections):        
        if Sec==0:
            Sec_Eulersindx=np.where((Eulers[:,1]>=(90-DSec/2)) | (Eulers[:,1]<(Sec+DSec/2)))[0]
        else:
            Sec_Eulersindx=np.where((Eulers[:,1]>=(Sec-DSec/2)) & (Eulers[:,1]<(Sec+DSec/2)))[0]
        phi1= Eulers[Sec_Eulersindx,0]
        phi2= Eulers[Sec_Eulersindx,2]
        phi1s.append(phi1); phi2s.append(phi2)
        if mode=='Density_Map':                                    
            xm,ym,Density= Funcs.Points2Dens2(phi1,phi2,MS,MSx,MSy)
            maxv= np.amax(Density)                
            maxvOfall=max(maxvOfall,maxv)
            xms.append(xm); yms.append(ym); Densitys.append(Density)
    contours=[] ; levels=[]
    sum=0
    for i,Sec in enumerate(Sections):
        Btm=0.5 if i<=8 else 0    
        Lft=i   if i<=8 else i-9    
        wd=(1-0.02)/9
        ax=fig.add_axes([0.02+Lft*wd,Btm,wd, 0.5]) 
        sum += len(phi1) 
        if mode=='Point_Projection':                  
            ax.scatter(phi1s[i],phi2s[i])       
        if mode=='Density_Map':                     
            xm,ym,Density= xms[i], yms[i], Densitys[i] 
            cs= ax.contourf(xm,ym,Density, levels=10+1, vmin=0, vmax=maxvOfall, cmap=cmapx,extend='min')    
            contours.append(cs.collections)
            levels.append(cs.levels)
        ax.xaxis.set_ticks_position('top')
        if i==0:
            ax.set_xticks(np.linspace(0,90,7))
            ax.set_yticks(np.linspace(0,90,7))
        if i>0: ax.axis('off') 
        ax.set_aspect('equal') 
        ax.set_xlim([0,90]) ; ax.set_ylim([90,0])
        txt='Phi='+str(int(Sec))            
        ax.text(35,97,txt,fontsize=12)
        Rect1 = plt.Rectangle((0, 0), 90, 90, color='black', fill=0)
        ax.add_artist(Rect1)
    print('sum=',sum)
    fig.savefig('ODF.png')   
    # adding image to GUI
    self.pixmap = QtGui.QPixmap('ODF.png')      
    self.Pic_WDG2.setPixmap(self.pixmap)
    self.Pic_WDG2.setScaledContents(True)    

    figX=plt.figure(figsize=(8,8),dpi=100) 
    axX=figX.add_subplot(111,projection='3d')    
    verts=[] ; zs=[] ; clrs=[]
    for i,Sec in enumerate(Sections):
        lvls=len(levels[i])
        for j,collection in enumerate(contours[i][1:]):                    
            for path in collection.get_paths():
                for ar in path.to_polygons():
                    clri=list(cmapx(levels[i][j]/maxvOfall))
                    clri[3]=(j+1)/lvls
                    clrs.append(tuple(clri))
                    zs.append(Sec)
                    verts.append(ar)

    poly = PolyCollection(verts,facecolors=clrs)
    poly.set_alpha(0.7)
    axX.add_collection3d(poly, zs=zs, zdir='z')
    axX.set_xlim([0,90]) ; axX.set_ylim([90,0]) ; axX.set_zlim([90,0])
    figX.show()
    


def Plot3D_BTN(self):
    PlotScatter3d(self)
    
    
def PlotScatter3d(self):
    Eulers=Funcs.Reduce_With_Sym(EulersAll)
    # 3D Scatter plot
    fig_scatter=plt.figure() 
    ax_scatter=fig_scatter.add_subplot(111, projection='3d',proj_type = 'ortho')
    # ColorMap for contour
    cmapx=plt.get_cmap('RdPu') # 'Blues' 'Reds' 'RdPu'
    # Density Mesh
    MS=10 # Number of Grids in each direction for Density mesh: [90 or 45 or 30 or 18 or 15 or 10]
    MSxyz= np.linspace(0,90-90/MS,MS)
    MSx,MSy,MSz = np.meshgrid(MSxyz,MSxyz,MSxyz)      
    
    # Determining Density in each mesh grid:
    Density= Funcs.Points2Dens3D(np.transpose(Eulers),MS)
    
    # Removing grids with 0 Density
    MSx=MSx.flat ; MSy=MSy.flat ; MSz=MSz.flat ; Density=Density.flat
    del_i=np.where(Density==0)[0]
    Density=np.delete(Density,del_i)
    MSx=np.delete(MSx,del_i) ; MSy=np.delete(MSy,del_i) ; MSz=np.delete(MSz,del_i)
    # Scaling Remaining non-Zero Density to be in range [0 to 1]
    Density=(Density-min(Density))/(max(Density)-min(Density))
    clr=cmapx(np.ones(len(Density))) ;  clr[:,3]=Density

    # Plottig
    #ax_scatter.scatter(MSx,MSy,MSz, color=clr)    
    xyz=MSx ; xyz=np.vstack((xyz,MSy)) ; xyz=np.vstack((xyz,MSz)) ; xyz=np.transpose(xyz)
    sz=np.ones((len(clr),3))*90/MS   
    
    pc = plotCubeAt(xyz, sizes=sz, colors=clr,edgecolor="k")
    ax_scatter.add_collection3d(pc)
    ax_scatter.set_xlim([0,90]); ax_scatter.set_ylim([0,90]); ax_scatter.set_zlim([0,90])
    ax_scatter.elev = 90 ; ax_scatter.azim = 0

    fig_scatter.show()
    fig_scatter.savefig('Scatter_Plot.png')
    

def cuboid_data(o, size=(1,1,1)):
    X = [[[0, 1, 0], [0, 0, 0], [1, 0, 0], [1, 1, 0]],
         [[0, 0, 0], [0, 0, 1], [1, 0, 1], [1, 0, 0]],
         [[1, 0, 1], [1, 0, 0], [1, 1, 0], [1, 1, 1]],
         [[0, 0, 1], [0, 0, 0], [0, 1, 0], [0, 1, 1]],
         [[0, 1, 0], [0, 1, 1], [1, 1, 1], [1, 1, 0]],
         [[0, 1, 1], [0, 0, 1], [1, 0, 1], [1, 1, 1]]]
    X = np.array(X).astype(float)
    for i in range(3):
        X[:,:,i] *= size[i]
    X += np.array(o)
    return X

def plotCubeAt(positions,sizes=None,colors=None, **kwargs):
    if not isinstance(colors,(list,np.ndarray)): colors=["C0"]*len(positions)
    if not isinstance(sizes,(list,np.ndarray)): sizes=[(1,1,1)]*len(positions)
    g = []
    for p,s,c in zip(positions,sizes,colors):
        g.append( cuboid_data(p, size=s) )
    return Poly3DCollection(np.concatenate(g),  linewidths=0.1,
                            facecolors=np.repeat(colors,6, axis=0), **kwargs)
    
def Get_PF_planes():
    return [[1,0,0],[1,1,0],[1,1,1]]

def choosen_cmap(self):
    if   self.actionPlasma.isChecked(): return 'plasma'
    elif self.actionViridis.isChecked(): return 'viridis'
    elif self.actionJet.isChecked(): return 'jet'
    elif self.actionRainbow.isChecked(): return 'rainbow'
    elif self.actionCool.isChecked(): return 'cool'
    
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)

    #Q=Funcs.Euler2Rot([0,-45,125],'deg')
    #angg=Funcs.Rot2Euler(Q,'deg') 

    MainWindow.setFixedSize(MainWindow.size()) #!# Disable Resizing
    MainWindow.show()       
    sys.exit(app.exec_())

