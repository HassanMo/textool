

# TexTool
Texture Analysis Tool
![RVE](Screenshot.PNG)

## Getting started

- [x] Implemented:
  - [x] Generating Random Texture (Euler Angles).
  - [x] Reading Euler angles from an external file.
    - [ ] being compatible with different delimited text files.
  - [x] Plotting Pole Figure
    - [x] With Point or Density distribution.
    - [x] By steriographic or equal-area projection.
    - [ ] Plotting in TD_ND or RD_ND space.
    - [ ] Fixing problem with lower density around the edge.
  - [x] Plotting ODF sections of Euler space.
    - [ ] Under Devolopment...
  - [x] Plotting 3D Euler space.
    - [ ] Being able to add lines or some indicators for the known texture components.
----------------------
- [x] To Implement:
  - [ ] Generating Texture for a certain texture component with a specific spread.
  - [ ] Also for an assymetric or Beta fiber.
  - [ ] Export generated Euler Angles.
  - [ ] Export as abaqus input file format.





