import numpy as np

def Euler_to_Rot(Ang,flag):
    # Computes the orientation matrix from global frame to lattice frame
    # in terms of the three Euler angles: phi1 (rotation about Z1), PHI
    # (rotation about X2) and phi2 (rotation about Z3) - Bunge notation
    if flag=='deg':
        Ang=np.deg2rad(Ang)        
    Q0=np.zeros(shape=(3,3))
    Q0[0,0] = np.cos(Ang[0])*np.cos(Ang[2])-np.sin(Ang[0])*np.cos(Ang[1])*np.sin(Ang[2])
    Q0[0,1] = np.sin(Ang[0])*np.cos(Ang[2])+np.cos(Ang[0])*np.cos(Ang[1])*np.sin(Ang[2])
    Q0[0,2] = np.sin(Ang[1])*np.sin(Ang[2])
    Q0[1,0] =-np.cos(Ang[0])*np.sin(Ang[2])-np.sin(Ang[0])*np.cos(Ang[1])*np.cos(Ang[2])
    Q0[1,1] =-np.sin(Ang[0])*np.sin(Ang[2])+np.cos(Ang[0])*np.cos(Ang[1])*np.cos(Ang[2])
    Q0[1,2] = np.sin(Ang[1])*np.cos(Ang[2])
    Q0[2,0] = np.sin(Ang[0])*np.sin(Ang[1])
    Q0[2,1] =-np.cos(Ang[0])*np.sin(Ang[1])
    Q0[2,2] = np.cos(Ang[1])
    return Q0

def Rot_to_Euler(Q,flag):
    # Computes the Euler angles associated with the orientation matrix
    # in terms of the three Euler angles: phi1 (rotation about Z1), PHI
    # (rotation about X2) and phi2 (rotation about Z3) - Bunge notation
    Ang=np.zeros(3)
    if (abs(Q[2,2])<1):
        Ang[1] = np.arccos(Q[2,2])
        STH    = np.sin(Ang[1])
        Ang[0] = np.arctan2(Q[2,0]/STH,-Q[2,1]/STH)
        Ang[2] = np.arctan2(Q[0,2]/STH,Q[1,2]/STH)
    else:
        Ang[0] = np.arctan2(Q[0,1],Q[0,0])
        Ang[1] = 0
        Ang[2] = 0
        Ang[0] += np.pi ; Ang[2] += np.pi
    if flag=='deg':
        Ang=np.rad2deg(Ang)          
    return Ang

def ROTMATAX(OMEGA,AXIS):
    # Computes the rotation matrix for a rotation about an axis AXIS with
    # an angle OMEGA: R = a@a + (I - a@a)cos w + (Ixa)sin w 
    ROT=np.zeros((3,3))
    ROT[0,0]=AXIS[0]*AXIS[0]*(1-np.cos(OMEGA))+np.cos(OMEGA)
    ROT[0,1]=AXIS[0]*AXIS[1]*(1-np.cos(OMEGA))-AXIS[2]*np.sin(OMEGA)
    ROT[0,2]=AXIS[0]*AXIS[2]*(1-np.cos(OMEGA))+AXIS[1]*np.sin(OMEGA)
    ROT[1,0]=AXIS[0]*AXIS[1]*(1-np.cos(OMEGA))+AXIS[2]*np.sin(OMEGA)
    ROT[1,1]=AXIS[1]*AXIS[1]*(1-np.cos(OMEGA))+np.cos(OMEGA)
    ROT[1,2]=AXIS[1]*AXIS[2]*(1-np.cos(OMEGA))-AXIS[0]*np.sin(OMEGA)
    ROT[2,0]=AXIS[0]*AXIS[2]*(1-np.cos(OMEGA))-AXIS[1]*np.sin(OMEGA)
    ROT[2,1]=AXIS[1]*AXIS[2]*(1-np.cos(OMEGA))+AXIS[0]*np.sin(OMEGA)
    ROT[2,2]=AXIS[2]*AXIS[2]*(1-np.cos(OMEGA))+np.cos(OMEGA)
    return ROT   

def RANDGEN(NR):
    # Generates a random texture file 
    # Random generation of array of numbers between 0 and 1
    angs=np.random.rand(NR,3)
    # Converting it to random angles
    angs[:,0]=360*angs[:,0]
    angs[:,1]=np.arccos(angs[:,1])*180/np.pi
    angs[:,2]=90*angs[:,2]
    return angs

def Omega(Omega0,Arr_Sgn,Arr_01):
    # Gaussian distribution with standard deviation Omega0
    # Arr_Sgn random array of 1s and -1s
    # Arr_01 random array of values between 0,1
    return Arr_Sgn * Omega0 * np.sqrt(-2*np.log(Arr_01))
vOmega = np.vectorize(Omega)

def GAUSSDIS(N_Ori,Omega0):  
    # Gaussian distribution with standard deviation Omega0
    # random array of 1s and -1s size N_Ori
    Arr_Sgn = np.random.randint(0,2,N_Ori)*2-1   
    # random array of values between 0,1 size N_Ori
    Arr_01 = np.random.rand(N_Ori)
    Omega = vOmega(Omega0,Arr_Sgn,Arr_01) 
    return  Omega

def Ax1(Delta,Psi):
    return np.sin(Delta) * np.cos(Psi)
vAx1 = np.vectorize(Ax1)
def Ax2(Delta,Psi):
    return np.sin(Delta) * np.sin(Psi)
vAx2 = np.vectorize(Ax2)
def Ax3(Delta,Psi):
    return np.cos(Delta)  
vAx3 = np.vectorize(Ax3)    

def ROTAXIS(N_Ori):
    # Generate N_Ori axis rotation following a uniform distribution      
    # Generate random values for (cos (delta)) within [0;1] and psi within
    # [0;2.Pi] - See Lequeu et al (1987)
    # Random generation for cos (delta) and psi
    Delta = np.arccos(np.random.rand(N_Ori))   
    Psi = 2 * np.pi * np.random.rand(N_Ori)
    Axis=np.zeros((N_Ori,3))
    Axis[:,0] = vAx1(Delta,Psi)
    Axis[:,1] = vAx2(Delta,Psi)
    Axis[:,2] = vAx3(Delta,Psi)
    return Axis     

# Arrays required in TEXTURE Subroutine for resolving orthtropic symmetry
Coef1=np.array([[0,   0, 0],
                [360, 0, -180],
                [180, 0, 0],
                [180, 0, -180]])
Coef2=np.array([[+1, +1, +1],
                [-1, +1, -1],
                [+1, +1, +1],
                [-1, +1, -1]])
Coef3=np.array([[+1, +1, +1],
                [-1, +1, +1],
                [-1, -1, +1],
                [+1, -1, +1]])   
Coef4=np.array([+1, -1, +1, -1])

def TEXTURE (N_Ori, EUL, Omega, Axis, IsSym):
    # Rotate the orientation matrix Q0 of the chosen texture component for
    # each pair (rotation axis/rotation angle) - Due to crystal symmetry
    # each rotation must also be performed 4 times, i.e. for each
    # equivalent orientation of the texture component
    # See Kuroda, Tvergaard - Int. J. Plasticity (2007)    
    TEX = np.zeros((N_Ori*4,3)) if IsSym else np.zeros((N_Ori,3))
    for Ni in range(N_Ori):        
        if not IsSym:
            # No orthotropic symmetry            
            # rotation about AXIS(x,y,z) with OMEGA
            Q0 = Euler_to_Rot(EUL,'deg')
            # Compute rotation matrix              
            AX=Axis[Ni,:]
            Omeg= np.deg2rad(Omega[Ni]) 
            RA = ROTMATAX(Omeg,AX)
            # Rotate orientation matrix Q0
            MAT = RA.T
            Q = np.matmul(Q0,MAT)
            # Compute corresponding Euler angles
            ANG = Rot_to_Euler(Q,'deg')
            TEX[Ni,:]=ANG
        if IsSym:
            # orthotropic symmetry            
            EUL0 = np.zeros(3)
            for i in range(4):
                EUL0[0]=Coef1[i,0]+Coef2[i,0]*EUL[0]
                EUL0[1]=Coef1[i,1]+Coef2[i,1]*EUL[1]
                EUL0[2]=Coef1[i,2]+Coef2[i,2]*EUL[2]                
                # rotation about AXIS(x,y,z) with OMEGA
                Q0 = Euler_to_Rot(EUL0,'deg')
                # Compute rotation matrix              
                AX=np.multiply(Axis[Ni,:],Coef3[i,:])
                Omeg=np.deg2rad(Omega[Ni]) * Coef4[i]
                RA = ROTMATAX(Omeg,AX)
                # Rotate orientation matrix Q0
                MAT = RA.T
                Q = np.matmul(Q0,MAT)
                # Compute corresponding Euler angles
                ANG = Rot_to_Euler(Q,'deg')
                TEX[4*Ni+i,:]=ANG               
    return TEX  
    
def USERTEX(N_Ori, EUL, Omega0,IsSym):
    # Generates user-defined textures
    # Generation of a texture made of Gaussian distributions & around ideal components.
    # Omega0 the scatter value in degress
    # Gaussian distribution with standard deviation Omega0:
    Omega = GAUSSDIS(N_Ori, Omega0)
    # Generate NORIEN rotation axes following a uniform distribution
    Axis = ROTAXIS(N_Ori)
    # Create the texture
    TEX = TEXTURE (N_Ori, EUL, Omega, Axis, IsSym)
    return TEX

